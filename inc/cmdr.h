#ifndef __TRI4_CMDR_INCL__
#define __TRI4_CMDR_INCL__

#include <cmdr/exceptions.h>
#include <cmdr/manipulators.h>
#include <cmdr/key.h>
#include <cmdr/groups.h>
#include <cmdr/arg.h>
#include <cmdr/parser.h>
#include <cmdr/helpers.h>
#include <cmdr/results.h>

#endif