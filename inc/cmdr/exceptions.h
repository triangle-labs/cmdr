#ifndef __TRI4_CMDR_EXCEPTIONS_INCL__
#define __TRI4_CMDR_EXCEPTIONS_INCL__

#include <exception>

namespace CMDR {
	namespace Exception {
		class KeyDuplicate : public std::exception {
			virtual const char* what() const throw();
		};
		class KeyConflict : public std::exception {
			virtual const char* what() const throw();
		};
		class NotEnoughKeys : public std::exception {
			virtual const char* what() const throw();
		};
		class InvalidKey : public std::exception {
			virtual const char* what() const throw();
		};
		class TooManyArgsObjects : public std::exception {
			virtual const char* what() const throw();
		};
		class TooManyStringObjects : public std::exception {
			virtual const char* what() const throw();
		};
		class StringAndArgsConflict : public std::exception {
			virtual const char* what() const throw();
		};
		class TooManyArgs : public std::exception {
			virtual const char* what() const throw();
		};
		class UnknownKeys : public std::exception {
			virtual const char* what() const throw();
		};
		class BindConflict : public std::exception {
			virtual const char* what() const throw();
		};
		class MixedAndLogicConflict : public std::exception {
			virtual const char* what() const throw();
		};
	}
}

#endif