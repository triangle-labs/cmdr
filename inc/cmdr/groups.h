#ifndef __TRI4_CMDR_GROUPS_INCL__
#define __TRI4_CMDR_GROUPS_INCL__

#include <vector>
#include <initializer_list>

#include <cmdr/key.h>
#include <cmdr/arg.h>
#include <cmdr/manipulators.h>

namespace CMDR {
	namespace Group {

		class Base {
		public:
			std::vector <CMDR::Key> keys;

			Base();
			Base(std::initializer_list<Key>);
		};

		// all keys under this group will be required
		class Required : public Base {
		public:
			Required(std::initializer_list<Key> list);
		};

		// all keys under this group will be optional
		// actually, it does nothing, only for nice-looking structure
		class Optional : public Base {
		public:
			Optional(std::initializer_list<Key> list) : Base(list) {};
		};

	}
}

#endif