#ifndef __TRI4_CMDR_MANIP_INCL__
#define __TRI4_CMDR_MANIP_INCL__

#include <string>

namespace CMDR {
	namespace Manip {

		class Base {
		private:
			std::string value;

		public:
			Base(std::string);
			Base(int);

			void set(std::string);
			void set(int);

			std::string getString() const;
			int getInt() const;

			operator std::string() const;
			operator int() const;
		};

		class LongKey : public Base {
		public:
			LongKey(std::string s) : Base(s) {}
		};
		class ShortKey : public Base {
		public:
			ShortKey(std::string s) : Base(s) {}
			ShortKey(char c) : Base(std::to_string(c)) {}
		};
		class Desc : public Base {
		public:
			Desc(std::string s) : Base(s) {}
		};
		class Required : public Base {
		public:
			Required(int r = 1) : Base(r) {}
		};
		class Logic : public Base {
		public:
			Logic(int l = 1) : Base(l) {}
		};
		class Mixed : public Base {
		public:
			Mixed(int l = 3) : Base(l) {}
		};
		class Default : public Base {
		public:
			Default(std::string d = "") : Base(d) {}
			Default(int d = 0) : Base(d) {}
		};

		// for Group only
		class Bind : public Base {
		public:
			Bind(std::string b = "") : Base(b) {}
		};
		class Limit : public Base {
		public:
			Limit(int b = -1) : Base(b) {}
		};

	}
}

#endif