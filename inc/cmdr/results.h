#ifndef __TRI4_CMDR_RESULTS_INCL__
#define __TRI4_CMDR_RESULTS_INCL__

#include <string>
#include <vector>
#include <map>

namespace CMDR {

	class Result {
	private:
		std::string value;

	public:
		Result(std::string = "");

		Result& operator=(std::string);
		std::string operator()();

		// integer conversion operators
		operator int();
		operator long int();
		operator long long();
		operator unsigned long();
		operator unsigned long long();

		// float conversion operators
		operator float();
		operator double();
		operator long double();

		// string conversion operators
		operator std::string();
		operator const char*();

	};

	class Results {
	private:
		std::map <std::string, Result> _values;
		std::vector <Result> _args;

	public:
		Results();

		Results& add(std::string, Result);
		Results& add(Result);
		Results& add(std::string, std::string);
		Results& add(std::string);

		const std::map <std::string, Result> values();
		const std::vector <Result> args();

		void erase(std::string);
		void erase(int);

		int count(std::string);

		// int count(std::string);
		int size(int = 2); // 1 - map, 2 - vector, 3 - map+vector
		void clear();

		Result& operator[](std::string);
		Result& operator[](int);

	};


}


#endif