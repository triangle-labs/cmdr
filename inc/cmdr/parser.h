#ifndef __TRI4_CMDR_PARSER_INCL__
#define __TRI4_CMDR_PARSER_INCL__

#include <vector>
#include <map>
#include <string>
#include <initializer_list>
#include <regex>
#include <sstream>

#include <iostream>

#include <cmdr/key.h>
#include <cmdr/groups.h>
#include <cmdr/exceptions.h>
#include <cmdr/results.h>

namespace CMDR {

	class Parser {

	private:
		std::vector <Key> keys;
		std::vector <std::string> binds;
		int args_enabled, args_required, args_limit;
		int string_enabled, string_required; std::string string_bind;

		Results _results;

	public:

		// std::map <std::string, std::string> values;
		// std::vector <std::string> args;

		Results results() const;

		Parser(std::initializer_list <Key> keys = {});

		void add(Key);
		void add(Arg);
		void add(Args);
		void add(String);

		void bind(unsigned int, std::string);

		void clearValues();
		void clearKeys();

		std::string help();
		int _getLongestName();
		int _getLongestDesc();
		std::string _getNames(Key&);

		std::string _clearInput(std::string);
		std::string _clearWS(std::string);
		std::string _clearString(std::string);
		std::string _extractArg(std::string*);

		Results parse(std::string);


		// std::string operator[](std::string);

		Results operator<<(std::string);

		Parser& operator<<(Key k);
		Parser& operator<<(Group::Base g);
		Parser& operator<<(Arg a);
		Parser& operator<<(Args a);
		Parser& operator<<(String a);

	};

}

#endif
