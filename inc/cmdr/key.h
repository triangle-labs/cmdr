#ifndef __TRI4_CMDR_KEY_INCL__
#define __TRI4_CMDR_KEY_INCL__

#include <string>
#include <map>
#include <vector>

#include <cmdr/manipulators.h>
#include <cmdr/exceptions.h>

namespace CMDR {

	class Key {
	private:
		uint8_t required;
		uint8_t logic;
		uint8_t mixed;
		std::string long_key, short_key, desc, default_value;

	public:
		Key(std::string long_key="");

		std::string getName();
		std::string getKeyName();
		std::string getNames();

		Key& LongKey(std::string);
		Key& ShortKey(std::string);
		Key& Desc(std::string);
		Key& Default(std::string);
		Key& Required(int);
		Key& Logic(int);
		Key& Mixed(int);

		std::string getLongKey() const;
		std::string getShortKey() const;
		std::string getDesc() const;
		std::string getDefault() const;
		int isRequired() const;
		int isLogic() const;
		int isMixed() const;

		operator Manip::LongKey() const;
		operator Manip::ShortKey() const;
		operator Manip::Desc() const;
		operator Manip::Default() const;
		operator Manip::Required() const;
		operator Manip::Logic() const;
		operator Manip::Mixed() const;

		Key& operator<<(const Manip::LongKey &rhs);
		Key& operator<<(const Manip::ShortKey &rhs);
		Key& operator<<(const Manip::Desc &rhs);
		Key& operator<<(const Manip::Default &rhs);
		Key& operator<<(const Manip::Required &rhs);
		Key& operator<<(const Manip::Logic &rhs);
		Key& operator<<(const Manip::Mixed &rhs);
	};

}

#endif
