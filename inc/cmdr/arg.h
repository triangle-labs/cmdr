#ifndef __TRI4_CMDR_ARG_INCL__
#define __TRI4_CMDR_ARG_INCL__

#include <string>

#include <cmdr/manipulators.h>

namespace CMDR {

	// one binded argument
	class Arg {
	public:
		std::string bind;

		Arg(std::string _a) : bind(_a) {}
		CMDR::Arg& operator<<(const CMDR::Manip::Bind&);
	};

	// group of non-binded arguments (represented as vector)
	// if limit==0 - unlimited, -1 - limit=required, 0 - unlimited
	class Args {	
	public:
		int limit;
		int required;

		Args(unsigned int _n = 0) : limit(_n), required(0) {}
		CMDR::Args& operator<<(const CMDR::Manip::Required&);
	};

	class String {
	public:
		std::string bind;
 		int required;

		String(std::string _b) : bind(_b), required(0) {}
		CMDR::String& operator<<(const CMDR::Manip::Bind&);
		CMDR::String& operator<<(const CMDR::Manip::Required&);
	};

}

#endif
