#ifndef __TRI4_CMDR_HELPERS_INCL__
#define __TRI4_CMDR_HELPERS_INCL__

#include <cmdr/parser.h>
#include <cmdr/key.h>
#include <cmdr/groups.h>

namespace CMDR {

	namespace Helper {
		// empty template, do nothing
		void addElement(CMDR::Parser* p);
		// empty template, do nothing
		void addElement(CMDR::Key* p);

		// add one element
		template <typename T, typename Current>
		void addElement(T* p, Current t);

		// add one element and then repeat
		template <typename T, typename First, typename ... Rest>
		void addElement(T* p, First first, Rest... rest);

	}

	namespace Create {
		template <typename ... Args>
		CMDR::Parser& Parser(Args... args);

		template <typename ... Args>
		CMDR::Key& Key(Args... args);
	}

}

#include <helpers.tpl.cpp>

#endif