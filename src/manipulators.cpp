#include <cmdr/manipulators.h>

CMDR::Manip::Base::Base(int value){
	set(value);
}

CMDR::Manip::Base::Base(std::string value){
	set(value);
}

void CMDR::Manip::Base::set(std::string value){
	this->value = value;
}

void CMDR::Manip::Base::set(int value){
	this->value = std::to_string(value);
}

std::string CMDR::Manip::Base::getString() const {
	return value;
}

int CMDR::Manip::Base::getInt() const {
	return std::stol(value);
}

CMDR::Manip::Base::operator std::string() const {
	return getString();
}

CMDR::Manip::Base::operator int() const {
	return getInt();
}