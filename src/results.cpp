#include <cmdr/results.h>

CMDR::Result::Result(std::string val){
	value = val;
}

CMDR::Result& CMDR::Result::operator=(std::string val){
	value = val;
	return *this;
}

std::string CMDR::Result::operator()(){
	return value;
}

CMDR::Result::operator int(){
	return std::stoi(value);
}
CMDR::Result::operator long int(){
	return std::stol(value);
}
CMDR::Result::operator long long(){
	return std::stoll(value);
}
CMDR::Result::operator unsigned long(){
	return std::stoul(value);
}
CMDR::Result::operator unsigned long long(){
	return std::stoull(value);
}
CMDR::Result::operator float(){
	return std::stof(value);
}
CMDR::Result::operator double(){
	return std::stod(value);
}
CMDR::Result::operator long double(){
	return std::stold(value);
}
CMDR::Result::operator std::string(){
	return value;
}
CMDR::Result::operator const char*(){
	return value.c_str();
}


CMDR::Results::Results() {}

CMDR::Results& CMDR::Results::add(CMDR::Result r){
	_args.push_back(r);
}
CMDR::Results& CMDR::Results::add(std::string bind, CMDR::Result r){
	_values[bind] = r;
}
CMDR::Results& CMDR::Results::add(std::string s){
	_args.push_back(CMDR::Result(s));
}
CMDR::Results& CMDR::Results::add(std::string bind, std::string r){
	_values[bind] = CMDR::Result(r);
}

int CMDR::Results::size(int flag){
	int s = 0;
	if (flag & 1) s+=_values.size();
	if (flag & 2) s+=_args.size();
	return s;
}
void CMDR::Results::clear(){
	_args.clear();
	_values.clear();
}

CMDR::Result& CMDR::Results::operator[](std::string s){
	return _values[s];
}
CMDR::Result& CMDR::Results::operator[](int i){
	return _args[i];
}

const std::map <std::string, CMDR::Result> CMDR::Results::values(){
	return _values;
}
const std::vector <CMDR::Result> CMDR::Results::args(){
	return _args;
}

void CMDR::Results::erase(int i){
	_args.erase(_args.begin()+i);
}
void CMDR::Results::erase(std::string s){
	_values.erase(s);
}

int CMDR::Results::count(std::string s){
	return _values.count(s);
}