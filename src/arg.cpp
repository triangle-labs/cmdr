#include <cmdr/arg.h>

CMDR::Arg& CMDR::Arg::operator<<(const CMDR::Manip::Bind &rhs){
	bind = (std::string)rhs;
	return *this;
}

CMDR::Args& CMDR::Args::operator<<(const CMDR::Manip::Required &rhs){
	required = (int)rhs;
	return *this;
}

CMDR::String& CMDR::String::operator<<(const CMDR::Manip::Bind &rhs){
	bind = (std::string)rhs;
	return *this;
}

CMDR::String& CMDR::String::operator<<(const CMDR::Manip::Required &rhs){
	required = (int)rhs;
	return *this;
}
