#include <cmdr/parser.h>

CMDR::Parser::Parser(std::initializer_list <Key> keys){
	args_enabled = 0;
	args_required = 0;
	args_limit = -1;
	string_enabled = 0;
	string_required = 0;
	for (auto i = keys.begin(); i != keys.end(); ++i) {
		add(*i);
	}
}

void CMDR::Parser::add(CMDR::Key k){
	if (k.getNames() == ""){
		throw Exception::InvalidKey();
	}
	if (std::any_of(keys.begin(), keys.end(), [&](Key &key){
		return (key.getLongKey()==k.getLongKey() || key.getShortKey()==k.getShortKey())? 1 : 0;
	})) throw Exception::KeyDuplicate();
	this->keys.push_back(k);
	std::string shorted; shorted.push_back(k.getLongKey()[0]);
	if (k.getShortKey()=="" && !std::any_of(this->keys.begin(), this->keys.end(), [&](Key& key){
		return key.getShortKey()==shorted? 1 : 0;
	})){
		this->keys.back() << Manip::ShortKey(shorted);
	}
}

void CMDR::Parser::add(CMDR::Arg a){
	args_enabled=1;
	for (auto k: keys) if (k.getName()==a.bind) throw Exception::BindConflict();
	binds.push_back(a.bind);
	args_required++;
}

void CMDR::Parser::add(CMDR::Args a){
	args_enabled=1;
	args_required+=a.required;
	if (args_limit==0 && a.limit==0) throw Exception::TooManyArgsObjects();
	if (string_enabled && a.limit==0) throw Exception::StringAndArgsConflict();
	args_limit = a.limit;
}

void CMDR::Parser::add(CMDR::String s){
	string_enabled? throw Exception::StringAndArgsConflict() : string_enabled=1;
	if (args_limit==0) throw Exception::StringAndArgsConflict();
	string_bind = s.bind;
	string_required = s.required;
}

void CMDR::Parser::clearKeys(){
	keys.clear();
	binds.clear();
}

void CMDR::Parser::clearValues(){
	_results.clear();
}

void CMDR::Parser::bind(unsigned int num, std::string b){
	for (auto k: keys){
		if (k.getName()==b) throw Exception::BindConflict();
	}
	binds[num]=b;
}

// std::string CMDR::Parser::operator[](std::string key){
// 	return values[key];
// }

int CMDR::Parser::_getLongestName(){
	int ret = 0;
	for(auto key: keys){
		int len = key.getNames().length();
		if (len>ret) ret = len;
	}
	return ret;
}

int CMDR::Parser::_getLongestDesc(){
	int ret = 0;
	for(auto key: keys){
		int len = key.getDesc().length();
		if (len>ret) ret = len;
	}
	return ret;
}

std::string CMDR::Parser::help(){
	std::string ret, names, desc;
	int max_name_len = _getLongestName(), cur_name_len;
	int max_desc_len = _getLongestDesc(), cur_desc_len;
	std::string ret_req, ret_opt, ret_arg;
	std::string* current;

	ret = "NANE"; // there is no place for actual name in parser itself now so yeah
	
	for (auto key: keys){
		if (key.isRequired()) {
			ret += " " + key.getKeyName();
		} else {
			ret += " [" + key.getKeyName() + "]";
		}

		current = key.isRequired()? &ret_req : &ret_opt;

		names = key.getNames();
		cur_name_len = names.length();
		*current+=names;
		for(auto i=0; i<(max_name_len-cur_name_len+2); i++) *current+=" ";

		if (key.isMixed()){
			*current+="[mixed]";
		} else if (key.isLogic()) {
			*current+="[logic]";
		} else {
			*current+="[value]";
		}
		for (auto i=0; i<2; i++) *current+=" ";

		desc = key.getDesc();
		cur_desc_len = desc.length();
		*current+=desc;
		for(auto i=0; i<(max_desc_len-cur_desc_len+2); i++) *current+=" ";

		if (key.getDefault()!="") *current+="Default value: \""+key.getDefault()+"\"";
		*current += "\n";
	}
	
	if (args_enabled){
		if (args_required) {
			int i = args_required;
			for (auto bind: binds) {
				ret += " " + bind;
				--i;
			}
			ret += " +" + std::to_string(i) + "arg" + (i!=1?"s":"");
			ret_arg = std::to_string(args_required) + " required";
		}
		if (args_limit == 0) {
			ret += " [args...] ";
			ret_arg+= (ret_arg!=""? ", " : "");
			ret_arg+= "unlimited";
		} else if (args_limit!=-1 && args_limit+binds.size()!=args_required){
			int optargs = args_limit-args_required+binds.size();
			ret += " [up to " + std::to_string(optargs) + " arg" + (optargs!=1?"s":"") + "] ";;
			ret_arg+= (ret_arg!=""? ", +" : "");
			ret_arg+= std::to_string(optargs) + " accepted.";
		}
	}
	if (string_enabled){
		ret += (string_required?"":" [");
		ret += string_bind;
		ret += (string_required?"":"]");
	}

	ret+="\n────────\nRequired:\n" + ret_req;
	ret += "────────\nOptional:\n" + ret_opt;
	if (args_enabled)
	ret += "────────\nArguments:\n" + ret_arg;
	if (string_enabled){
		ret +="\nString \"" + string_bind + "\" is ";
		ret += string_required? "required." : "optional.";
	}

	return ret;
}

CMDR::Results CMDR::Parser::parse(std::string input){
	clearValues();

	std::string input_stripped = input;

	int required = 0;

	std::string name, re, re_name;

	for (auto key: keys){
		if (key.isRequired()) {
			required++;
		}

		name = key.getName();
		if (key.getLongKey()==""){ // only short key
			re_name = "-"+name;
		} else if (key.getShortKey()==""){ // only long key
			re_name = "--"+name;
		} else { // both keys
			re_name = "(?:--"+key.getLongKey()+"|-"+key.getShortKey()+")";
		}

		if (key.isMixed()){
			re = re_name + "(?:=\"((?:[^\"\\\\]|\\\\.)*)\"|($| ))";
		} else if (key.isLogic()){
			re = re_name + "( |$)";
		} else {
			re = re_name + "(?: |=)(?:\"((?:[^\"\\\\]|\\\\.)*)\"|((?:[^\"\\\\])*?))(?:$| )";
		}
		
		std::vector<std::string> matches;

		std::smatch m;
		std::regex_match(input, m, std::regex(re));
		while (std::regex_search(input_stripped, m, std::regex(re))) {
			for (auto i=1; i<m.size(); i++){
				if (m[i]!="") {
					if (m[i]==" " && key.isMixed()) { matches.push_back(""); }
					else matches.push_back(m[i]);
				}
			}
			input_stripped = m.prefix().str() + m.suffix().str();
		}

		if (matches.size()<1) {
			continue;
		}

		if (matches.size()>1) {
			throw Exception::KeyDuplicate();
		}
		if (_results.count(name)){
			throw Exception::KeyConflict();
		}

		if (key.isMixed()) {
			if (matches[matches.size()-1] == "") { // logic
				_results.add(name, "true");
				key.Mixed(1);
			} else { // value
				_results.add(name, _clearString(matches[0]));
				key.Mixed(2);
			}
		} else {
			_results.add(name, key.isLogic()? "true" : matches[0]);
		}

		if (key.isRequired()) required--;
	}

	if (required!=0) throw Exception::NotEnoughKeys();

	// add default values for keys not in input
	for (auto k: keys){
		if (k.getDefault()!="" && _results.count(k.getName())==0) {
			auto def = k.getDefault();
			_results.add(k.getName(), (k.isLogic() || k.isMixed())? (def=="0"||def==""? "false" : "true") : def);
		}
	}

	// temp solution based on previous idea
	std::string input_cleared = _clearInput(input_stripped);
	if (input_stripped != input_cleared) throw Exception::UnknownKeys();

	std::vector<std::string> tokens;
	{
		std::string args_string = input_cleared;
		std::string a=_extractArg(&args_string);
		while (a!=""){
			tokens.push_back(a);
			a = _extractArg(&args_string);
		}
	}

	if (args_enabled){

		if (args_required>tokens.size()) throw Exception::NotEnoughKeys();
		if (args_limit>0 && string_enabled==0 && args_limit+binds.size()<tokens.size()) throw Exception::TooManyArgs();

		for (auto i=0; i<binds.size(); i++){
			_results.add(binds[i], _clearString(tokens[0]));
			// values[binds[i]]=_clearString(tokens[0]);
			tokens.erase(tokens.begin());
			_extractArg(&input_cleared);
		}

		for (auto i=0; i<args_limit; i++) {
			_results.add(_clearString(tokens[0]));
			// this->args.push_back(_clearString(tokens[0]));
			tokens.erase(tokens.begin());
			_extractArg(&input_cleared);
		}
	}

	if (string_enabled){
		std::string str_value = _clearString(input_cleared);
		if (string_required && str_value == "") throw Exception::NotEnoughKeys();
		if (str_value!="") _results.add(string_bind, str_value);
	}

	return _results;

}

CMDR::Results CMDR::Parser::operator<<(std::string input){
	return parse(input);
}

CMDR::Parser& CMDR::Parser::operator<<(CMDR::Key k){
	add(k);
}

CMDR::Parser& CMDR::Parser::operator<<(CMDR::Group::Base g){
	for (auto a: g.keys){
		add(a);
	}
}

CMDR::Parser& CMDR::Parser::operator<<(CMDR::Arg a){
	add(a);
}

CMDR::Parser& CMDR::Parser::operator<<(CMDR::Args a){
	add(a);
}

CMDR::Parser& CMDR::Parser::operator<<(CMDR::String s){
	add(s);
}


std::string CMDR::Parser::_extractArg(std::string *input){
	std::smatch m;
	std::string re = "(?:\"([^\"\\\\]*?(?:\\\\.[^\"\\\\]*?)*?)\"|([^\\s]*?)(?: |$))";
	if(std::regex_search (*input,m,std::regex(re))) {
		std::string result = "";
		for (auto i=1; i<m.size(); i++){
			if (m[i]!=""){
				result=m[i];
				break;
			}
		}
		*input = _clearWS(m.prefix().str() + m.suffix().str());
		return _clearWS(result);
	} else {
		return "";
	}
}

std::string CMDR::Parser::_clearInput(std::string input){
	std::regex re("(^| )-{1,2}.*?(?: |=)(?:\"(.*?)\"|([^\\s]*?)(?: |$))");
	return std::regex_replace(input, re, "");
}

std::string CMDR::Parser::_clearWS(std::string inp){
	return std::regex_replace(inp, std::regex("^ *| *$"), "");
}

std::string CMDR::Parser::_clearString(std::string inp){
	return std::regex_replace(inp, std::regex("\\\\\""), "\"");
}
