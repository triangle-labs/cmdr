#include <cmdr/exceptions.h>

const char* CMDR::Exception::KeyDuplicate::what() const throw(){
	return "Input string / Parser contains 2 or more keys with same name";
}

const char* CMDR::Exception::KeyConflict::what() const throw(){
	return "Some keys conflict with each other";
}

const char* CMDR::Exception::NotEnoughKeys::what() const throw(){
	return "Input don't contain all required keys";
}

const char* CMDR::Exception::InvalidKey::what() const throw(){
	return "Key must contain short or long key";
}

const char* CMDR::Exception::TooManyArgsObjects::what() const throw(){
	return "Parser can contain only 1 Args object";
}

const char* CMDR::Exception::TooManyStringObjects::what() const throw(){
	return "Parser can contain only 1 String object";
}

const char* CMDR::Exception::StringAndArgsConflict::what() const throw(){
	return "String conflicts with unlimited Args";
}

const char* CMDR::Exception::TooManyArgs::what() const throw(){
	return "Input contains too many args";
}

const char* CMDR::Exception::UnknownKeys::what() const throw(){
	return "Unknown keys in input";
}

const char* CMDR::Exception::BindConflict::what() const throw(){
	return "Bind conflicts with known keys/values";
}

const char* CMDR::Exception::MixedAndLogicConflict::what() const throw(){
	return "Key can be only mixed or logic";
}