template <typename ... Args>
CMDR::Parser& CMDR::Create::Parser(Args... args){
	CMDR::Parser *p = new CMDR::Parser{};
	CMDR::Helper::addElement<CMDR::Parser>(p, args...);
	return *p;
}
template <typename ... Args>
CMDR::Key& CMDR::Create::Key(Args... args){
	CMDR::Key *k = new CMDR::Key();
	CMDR::Helper::addElement<CMDR::Key>(k, args...);
	return *k;
}

template <typename T, typename Current>
void CMDR::Helper::addElement(T* k, Current t){
	(*k) << t;
}

template <typename T, typename First, typename ... Rest>
void CMDR::Helper::addElement(T* k, First first, Rest... rest){
	CMDR::Helper::addElement<T>(k, first);
	CMDR::Helper::addElement<T>(k, rest...);
}