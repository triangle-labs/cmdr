#include <cmdr/groups.h>

CMDR::Group::Base::Base() {}

CMDR::Group::Base::Base(std::initializer_list<CMDR::Key> list) {
	for (auto l: list){
		keys.push_back(l);
	}
}

CMDR::Group::Required::Required(std::initializer_list<CMDR::Key> list) {
	for (auto l: list){
		l << CMDR::Manip::Required();
		keys.push_back(l);
	}
}