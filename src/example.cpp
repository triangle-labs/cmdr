#include <iostream>
#include <string>

#include <cmdr.h>

using namespace std;
using namespace CMDR::Manip;
using namespace CMDR;

int main(int argc, char const *argv[]){

	auto parser = Create::Parser(
		Group::Required({
			Key("key") << Desc("the first key"),
			Key("noshort") << Logic() << Desc("needs short key")
		}),
		Group::Optional({
			Key("testmix") << ShortKey("tm") << Mixed() << Desc("mixed key"),
			Key("mixkey") << Mixed(),
			Key() << ShortKey("o") << Desc("short key only") << Default(""),
			Key("long-key").Desc("that's the key"),
			Key("another-long-key") << ShortKey("a"),
			Key("fsingle").Desc("will not get anything").Default("its only value")
		}),
		Arg("first_arg"),
		Args(2) << Required(1),
		String("dat_string") << Required()
	);

	string input = "--long-key value Arg1 --key=\"value with spaces\" arg2 --another-long-key \"value with \\\"stuff\\\"\" arg_3 --mixkey=\"mix this\" -o=4 -n cool and new string";

	cout << parser.help() << endl << endl;
	cout << "Input: " << input << endl << endl;

	auto result = parser << input;

	cout << "Parsed keys:" << endl;
	for (auto it: result.values()) {
		cout << "\t" << it.first << ": " << it.second() << endl;
	}
	cout << "Parsed args:" << endl;
	for (auto it: result.args()) {
		cout << "\t" << it() << endl;
	}

	std::cout << std::endl;
	return 0;
}
