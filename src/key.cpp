#include <cmdr/key.h>

CMDR::Key::Key(std::string long_key){
	this->long_key = long_key;
	short_key = "";
	desc = "";
	default_value = "";
	required = 0;
	mixed = 0;
	logic = 0;
}

std::string CMDR::Key::getName(){
	return long_key==""? short_key : long_key;
}

std::string CMDR::Key::getKeyName(){
	return long_key==""? "-"+short_key : "--"+long_key;
}

std::string CMDR::Key::getNames(){
	std::string ret="";
	std::vector <std::string> tmp;
	if (long_key!="") tmp.push_back("--"+long_key);
	if (short_key!="") tmp.push_back("-"+short_key);
	for (auto q: tmp) ret+=q+", ";
	ret.pop_back();ret.pop_back();
	return ret;
}

// setters
CMDR::Key& CMDR::Key::LongKey(std::string s){
	long_key = s;
	return *this;
}
CMDR::Key& CMDR::Key::ShortKey(std::string s){
	short_key = s;
	return *this;
}
CMDR::Key& CMDR::Key::Desc(std::string s){
	desc = s;
	return *this;
}
CMDR::Key& CMDR::Key::Default(std::string s){
	default_value = s;
	return *this;
}
CMDR::Key& CMDR::Key::Required(int r = 1){
	required = r;
	return *this;
}
CMDR::Key& CMDR::Key::Logic(int l = 1){
	if (mixed != 0) {
		throw Exception::MixedAndLogicConflict();
	}
	logic = l;
	if (default_value=="") default_value="0";
	return *this;
}
CMDR::Key& CMDR::Key::Mixed(int m = 3){
	if (logic != 0) {
		throw Exception::MixedAndLogicConflict();
	}
	mixed = m;
	if (default_value=="") default_value="0";
	return *this;
}

// getters
std::string CMDR::Key::getLongKey() const {
	return long_key;
}
std::string CMDR::Key::getShortKey() const {
	return short_key;
}
std::string CMDR::Key::getDesc() const {
	return desc;
}
std::string CMDR::Key::getDefault() const {
	return default_value;
}
int CMDR::Key::isRequired() const{
	return required;
}
int CMDR::Key::isLogic() const{
	return logic;
}
int CMDR::Key::isMixed() const{
	return mixed; // 0 -- not mixed
                  // 1 -- logic mixed
                  // 2 -- value mixed
                  // 3 -- undefined mixed
}


CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::LongKey &rhs){
	return LongKey(rhs);
}
CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::ShortKey &rhs){
	return ShortKey(rhs);
}
CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::Desc &rhs){
	return Desc(rhs);
}
CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::Default &rhs){
	return Default(rhs);
}
CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::Required &rhs){
	return Required(rhs);
}
CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::Logic &rhs){
	return Logic(rhs);
}
CMDR::Key& CMDR::Key::operator<<(const CMDR::Manip::Mixed &rhs){
	return Mixed(rhs);
}


CMDR::Key::operator CMDR::Manip::LongKey() const {
	return getLongKey();
}
CMDR::Key::operator CMDR::Manip::ShortKey() const {
	return getShortKey();
}
CMDR::Key::operator CMDR::Manip::Desc() const {
	return getDesc();
}
CMDR::Key::operator CMDR::Manip::Default() const {
	return getDefault();
}
CMDR::Key::operator CMDR::Manip::Required() const {
	return isRequired();
}
CMDR::Key::operator CMDR::Manip::Logic() const {
	return isLogic();
}
CMDR::Key::operator CMDR::Manip::Mixed() const {
	return isMixed();
}
