cmake_minimum_required(VERSION 2.8.7)

project(CMDR)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3 -s")

set (CMDR_SOURCES
	src/parser.cpp
	src/key.cpp
	src/helpers.cpp
	src/exceptions.cpp
	src/manipulators.cpp
	src/groups.cpp
	src/arg.cpp
	src/results.cpp
)

set (CMDR_INCLUDE_DIRS
	inc
	src
)

set (CMDR_LIBRARIES
)

add_executable(example src/example.cpp ${CMDR_SOURCES})
include_directories(${CMDR_INCLUDE_DIRS})
target_link_libraries(example ${CMDR_LIBRARIES})